const app = document.getElementById('container');

const container = document.createElement('div');
container.setAttribute('class', 'container');
//const logo = document.createElement('img');
//logo.src = 'logo.png';
//app.appendChild(logo);
app.appendChild(container);

var serviceChoice=document.querySelector('input[name="service"]:checked').value;
//console.log(serviceChoice);
var method = 'GET';
var url = 'https://localhost:8082/api/departments?flag=false';
//'http://localhost:8081/api/departments?flag=false'; // uncomment when deploying on local machine
//var url = 'http://employee-portal-1.us-e2.cloudhub.io/api/departments?flag=false'; // uncomment when deploying on cloud machine
var xhr = new XMLHttpRequest();

if ("withCredentials" in xhr) {

  // Check if the XMLHttpRequest object has a "withCredentials" property.
  // "withCredentials" only exists on XMLHTTPRequest2 objects.
  
  xhr.open(method, url, true);
  console.log('Sent Request to get Employees from '+serviceChoice+' Service.');
  //xhr.setRequestHeader('Content-Type', 'application/xml');
  xhr.onload = function () {
  var data = JSON.parse(this.response);
  //console.log(data.departments);
  const tabl = document.createElement('table')
  const head = document.createElement('thead')
  const row = document.createElement('tr')
  const empid = document.createElement('th')
  empid.setAttribute('class', 'card')
  empid.textContent = 'Department ID'
  const name = document.createElement('th')
  name.textContent = 'Department Name'
  name.setAttribute('class', 'card')
  row.appendChild(empid)
  row.appendChild(name)
  head.appendChild(row)
  tabl.appendChild(head)
  container.appendChild(tabl);
  const body = document.createElement('tbody'); // add table
  
  // add rows
  data.departments.forEach(dept => { 
	  const row1 = document.createElement('tr');
      body.appendChild(row1);
      const empid1 = document.createElement('td');
      empid1.textContent = dept.departmentId;
      const name1 = document.createElement('td');
      name1.textContent = dept.departmentName;
      
      row1.appendChild(empid1);
      row1.appendChild(name1);
      
      tabl.appendChild(body);
  })
  container.appendChild(tabl); 
  //console.log(data);
  }
  
  xhr.send();

} else if (typeof XDomainRequest != "undefined") {

  // Otherwise, check if XDomainRequest.
  // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
  console.log('Undefined');	
  xhr = new XDomainRequest();
  xhr.open(method, url);

} else {

  // Otherwise, CORS is not supported by the browser.
  console.log('Not Supported');	
  throw new Error('CORS not supported');

}
